import React, { Component } from 'react';
import './App.css';
import ModalAlertBuilder from "./containers/ModalAlertBuilder/ModalAlertBuilder";

class App extends Component {
  render() {
    return (
      <div className="App">
        <ModalAlertBuilder/>
      </div>
    );
  }
}

export default App;
