import React, {Component, Fragment} from 'react';
import './ModalAlertBuilder.css';
import Modal from "../../components/Modal/Modal";
import Alerts from "../../components/Alerts/Alerts";

class ModalAlertBuilder extends Component {
    state = {
        modalShow: false,
        isHide: false
    };

    showModal = () => {
      this.setState({modalShow: true});
    };

    dismiss = () => {
        this.setState({
            isHide: true
        })
    };

    closed = () => {
      this.setState({modalShow: false});
    };

    render() {
        return (
            <Fragment>
                <button onClick={this.showModal}>Click here</button>
                <Modal
                    show={this.state.modalShow}
                    closed={this.closed}
                    title="This is title for this modal window"
                >
                    <p>Content for this Modal window</p>
                </Modal>
                <Alerts
                    isHide={this.state.isHide}
                    type="warning"
                    dismiss={this.dismiss}
                >
                    This is a warning type alert
                </Alerts>
                <Alerts type="success">
                    This is a success type alert
                </Alerts>
                <Alerts
                    type="danger"
                >
                    This is a danger type alert
                </Alerts>
                <Alerts type="primary">
                    This is a primary type alert
                </Alerts>
            </Fragment>

        );
    }
}

export default ModalAlertBuilder;