import React from 'react';
import './Modal.css';
import Backdrop from "../Backdrop/Backdrop";

const Modal = props => {
    return (
        <div>
            <Backdrop show={props.show} onClick={props.close} />
            <div className="Modal" style={{display: props.show ? 'block' : 'none'}}>
                <h3 className="modal-title">{props.title}</h3>
                <button className="Close" onClick={props.closed}>Close</button>
                {props.children}
            </div>
        </div>
    );
};

export default Modal;