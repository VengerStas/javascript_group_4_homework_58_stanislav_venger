import React from 'react';
import './Alerts.css';

const Alerts = props => {
    return (
       !props.isHide ?  <div
           id="Alerts"
           className={props.type}
           onClick={props.close}
       >
           {props.children}
           {props.dismiss !== undefined ? <button className="alert-close" onClick={props.dismiss}>X</button> : null}
       </div> : null
    );
};

export default Alerts;